#!/bin/bash
set -e

arrayWords=(
"celery_beat.sh"
"celery_periodic_send_swift_payments.sh"
"celery_periodic_sqs_queue_mt_messages_swift_payments_receiver_task.sh"
"celery_queue_audit.sh"
"celery_queue_daemon_qus_send_swift_files.sh"
"celery_queue_get_ef3_payments.sh"
"celery_queue_send_qxt_task.sh"
"celery_queue_send_qxt_to_quantum_secondary.sh"
"celery_queue_settlement_amount_email_notification_task.sh"
"daemon_ef3_payments_notifications_consumer.sh"
"daemon_ledger_rate.sh"
"daemon_lxnx_password.sh"
"daemon_lxnx_polling.sh"
"daemon_lxnx.sh"
"daemon_payment_routing.sh"
"daemon_payments.sh"
"daemon_sentenial_get.sh"
"daemon_sqs_autoclient_mt_940_receiver.sh"
"daemon_sqs_autoclient_receiver.sh"
"daemons.sh"
"find_duplicated_migrations.sh"
"run_code_analysis_in_branch.sh"
"start-fxsuite"
"command="
)

echo "Number of items in original array: ${#arrayWords[*]}"

OUTPUT_CSV="output_fxs.csv"
OUTPUT_ALL_CSV="search_all_${OUTPUT_CSV}"

DEBUG=true
if [ -z "$1" ]; then
    DEBUG=true
fi
case ${DEBUG} in
  (true)    echo "DEBUG is true";;
  (false)   echo "DEBUG is false";;
esac

function echocolor() { # $i = string
	RED='\033[0;31m'
	NC='\033[0m' # No Color
    printf "${RED}$i${NC}\n"
}

source ./search_config.sh

COUNTER=1
for ix in ${!arrayWords[*]}
do
    i=${arrayWords[$ix]}
    echo "FXS Searching word [${i}]"
    COMMAND="sudo grep --include=${INCLUDE_EXT} --include=.bash_aliases -rn '/' --exclude-dir={${EXCLUDE_DIR}} -e \"${i}\""

    set +e
    echo "#inventory_atower_vars_ebury.production.fxs.txt "
    ack "$i" $HOME/code/fxs-ansible/inventory_atower_vars_ebury.production.fxs.txt > /tmp/OUTPUT_AT_01.${OUTPUT_CSV} &
    echo "#inventory_atower_vars_frontier.production.fxs.txt"
    ack "$i" $HOME/code/fxs-ansible/inventory_atower_vars_frontier.production.fxs.txt > /tmp/OUTPUT_AT_02.${OUTPUT_CSV} &


    echo "#fxs-ansible"
    ack "$i" $HOME/code/fxs-ansible > /tmp/OUTPUT_OCS_01.${OUTPUT_CSV} &
    echo "#edk"
    ack "$i" $HOME/code/edk >         /tmp/OUTPUT_OCS_02.${OUTPUT_CSV}  &
    echo "#ed2k"
    ack "$i"$HOME/code/ed2k >        /tmp/OUTPUT_OCS_03.${OUTPUT_CSV} &
    set +e

    echo "#staging"
    issh -C stagingfxs-1a "${COMMAND} $1" 2>&1 >       /tmp/OUTPUT_INSTACE_EBURY_DEVEL_01.${OUTPUT_CSV} &
    issh -C stagingfxs-1b "${COMMAND} $1" 2>&1 >       /tmp/OUTPUT_INSTACE_EBURY_DEVEL_02.${OUTPUT_CSV} &
    issh -C stagingfxscelery-1a "${COMMAND} $1" 2>&1 > /tmp/OUTPUT_INSTACE_EBURY_DEVEL_03.${OUTPUT_CSV} &

    echo "#prod"
    issh -C prodfxs-1a "${COMMAND} $1" 2>&1 >          /tmp/OUTPUT_INSTACE_EBURY_PROD_01.${OUTPUT_CSV} &
    issh -C prodfxs-1b "${COMMAND} $1" 2>&1 >          /tmp/OUTPUT_INSTACE_EBURY_PROD_02.${OUTPUT_CSV} &
    issh -C prodfxscelery-1a "${COMMAND}" 2>&1 >    /tmp/OUTPUT_INSTACE_EBURY_PROD_03.${OUTPUT_CSV} &

    echo "#prod"
    issh -C prodthefxfirmfxs "${COMMAND}" 2>&1 >                    /tmp/OUTPUT_INSTACE_FRONTIER_PROD_01.${OUTPUT_CSV} &

    echo "#prod"
    issh -C bosdemo.eburypartners.com "${COMMAND}" 2>&1 >           /tmp/OUTPUT_INSTACE_EBURY_DEMO_01.${OUTPUT_CSV} &

    cho "#prod"
    issh -C bos.ebury-api-edk-demo.ebury.rocks "${COMMAND}" 2>&1 >  /tmp/OUTPUT_INSTACE_API_DEMO_01.${OUTPUT_CSV} &

    wait

    OUTPUT_AT_01=`cat /tmp/OUTPUT_AT_01.${OUTPUT_CSV}`
    OUTPUT_AT_02=`cat /tmp/OUTPUT_AT_02.${OUTPUT_CSV}`

    OUTPUT_OCS_01=`cat /tmp/OUTPUT_OCS_01.${OUTPUT_CSV}`
    OUTPUT_OCS_02=`cat /tmp/OUTPUT_OCS_02.${OUTPUT_CSV}`
    OUTPUT_OCS_03=`cat /tmp/OUTPUT_OCS_03.${OUTPUT_CSV}`

    OUTPUT_INSTACE_EBURY_DEVEL_01=`cat /tmp/OUTPUT_INSTACE_EBURY_DEVEL_01.${OUTPUT_CSV}`
    OUTPUT_INSTACE_EBURY_DEVEL_02=`cat /tmp/OUTPUT_INSTACE_EBURY_DEVEL_02.${OUTPUT_CSV}`
    OUTPUT_INSTACE_EBURY_DEVEL_03=`cat /tmp/OUTPUT_INSTACE_EBURY_DEVEL_03.${OUTPUT_CSV}`

    OUTPUT_INSTACE_EBURY_PROD_01=`cat /tmp/OUTPUT_INSTACE_EBURY_PROD_01.${OUTPUT_CSV}`
    OUTPUT_INSTACE_EBURY_PROD_02=`cat /tmp/OUTPUT_INSTACE_EBURY_PROD_02.${OUTPUT_CSV}`
    OUTPUT_INSTACE_EBURY_PROD_03=`cat /tmp/OUTPUT_INSTACE_EBURY_PROD_03.${OUTPUT_CSV}`

    OUTPUT_INSTACE_FRONTIER_PROD_01=`cat /tmp/OUTPUT_INSTACE_FRONTIER_PROD_01.${OUTPUT_CSV}`
    OUTPUT_INSTACE_EBURY_DEMO_01=`cat /tmp/OUTPUT_INSTACE_EBURY_DEMO_01.${OUTPUT_CSV}`
    OUTPUT_INSTACE_API_DEMO_01=`cat /tmp/OUTPUT_INSTACE_API_DEMO_01.${OUTPUT_CSV}`


    #line headers
    if [ "$COUNTER" == "1" ]; then
        echocolor "Print headers..."
        echo -n "FILE;AT_01;AT_02"          >   ${OUTPUT_ALL_CSV}
        echo -n ";OCS_01;OCS_02;OCS_03"     >>  ${OUTPUT_ALL_CSV}
        echo -n ";INSTACE_EBURY_DEVEL_01;INSTACE_EBURY_DEVEL_02;INSTACE_EBURY_DEVEL_03"  >>  ${OUTPUT_ALL_CSV}
        echo -n ";INSTACE_EBURY_PROD_01;INSTACE_EBURY_PROD_02;INSTACE_EBURY_PROD_03"     >>  ${OUTPUT_ALL_CSV}
        echo -n ";INSTACE_FRONTIER_PROD_01" >>  ${OUTPUT_ALL_CSV}
        echo -n ";INSTACE_EBURY_DEMO_01"    >>  ${OUTPUT_ALL_CSV}
        echo -n ";INSTACE_API_DEMO_01"      >>  ${OUTPUT_ALL_CSV}
        echo ""                             >>  ${OUTPUT_ALL_CSV}
    fi

    #data
    echocolor "Print data for $i"
    echo -n "$i"                                                    >>  ${OUTPUT_ALL_CSV}
    echo -n ";'$OUTPUT_AT_01';'$OUTPUT_AT_02'"                      >>  ${OUTPUT_ALL_CSV}
    echo -n ";'$OUTPUT_OCS_01';'$OUTPUT_OCS_02';'$OUTPUT_OCS_03'"   >>  ${OUTPUT_ALL_CSV}
    echo -n ";'$OUTPUT_INSTACE_EBURY_DEVEL_01';'$OUTPUT_INSTACE_EBURY_DEVEL_02';'$OUTPUT_INSTACE_EBURY_DEVEL_03'"   >>  ${OUTPUT_ALL_CSV}
    echo -n ";'$OUTPUT_INSTACE_EBURY_PROD_01';'$OUTPUT_INSTACE_EBURY_PROD_02';'$OUTPUT_INSTACE_EBURY_PROD_03'"      >>  ${OUTPUT_ALL_CSV}
    echo -n ";'$OUTPUT_INSTACE_FRONTIER_PROD_01'"                   >>  ${OUTPUT_ALL_CSV}
    echo -n ";'$OUTPUT_INSTACE_EBURY_DEMO_01'"                      >>  ${OUTPUT_ALL_CSV}
    echo -n ";'$OUTPUT_INSTACE_API_DEMO_01'"                        >>  ${OUTPUT_ALL_CSV}
    echo ""                                                         >>  ${OUTPUT_ALL_CSV}

    let COUNTER=COUNTER+1
    echo "+Counter $COUNTER"

done

echocolor "END"

nohup libreoffice ${OUTPUT_ALL_CSV} &