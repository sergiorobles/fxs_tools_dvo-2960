#!/bin/bash
set -e

export INCLUDE_EXT="\*.{sh,bash,conf}"
export EXCLUDE_DIR="dev,boot,mnt,opt,vagrant,proc,lib,tmp,samba,share,usr,src,modprobe.d,dbus-1"
