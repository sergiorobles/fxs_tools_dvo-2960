#!/bin/bash
set -e

echo "FXS Seaching word [$1]"

if [ -z "$1" ]
  then
    echo "No argument supplied"
    echo "Parameters: $0 <wordToSearch> <debug>"
fi

debug=true
if [ -z "$2" ]
  then
    debug=true
fi


function echocolor() { # $1 = string
	RED='\033[0;31m'
	NC='\033[0m' # No Color
    printf "${RED}$1${NC}\n"
}


case $debug in
  (true)    echo "debug is true";;
  (false)   echo "debug is false";;
esac

source ./search_config.sh

COMMAND="sudo grep --include=${INCLUDE_EXT} --include=.bash_aliases -rn '/' --exclude-dir={${EXCLUDE_DIR}} -e \"$1\""

echo "Search [$COMMAND]"


set +e

echocolor "#AT ebury settings"
#frontierpay.production.fxs.deploy
## inventory : frontierpay.production.fxs
## extravar: branch_name: default
## script: deploy_amazon/deploy_fxs.yml
	OUTPUT_AT_01=$(ack "$1" $HOME/code/fxs-ansible/inventory_atower_vars_ebury.production.fxs.txt 2>&1)
	if $debug ; then echo "DEBUG[$OUTPUT_AT_01]" ; fi


echocolor "#AT frontier settings"
#ebury.production.fxs.deploy
#branch_name: default
#deploy_amazon/deploy_fxs.yml
	OUTPUT_AT_02=$(ack "$1" $HOME/code/fxs-ansible/inventory_atower_vars_frontier.production.fxs.txt 2>&1)
	if $debug ; then echo "DEBUG[${OUTPUT_AT_02}]" ; fi

echocolor "#OSC Ansible Job FXS"
	OUTPUT_OCS_01=$(ack "$1" $HOME/code/fxs-ansible 2>&1)
	if $debug ; then echo "DEBUG[${OUTPUT_OCS_01}]" ; fi

echocolor "#OSC EDK FXS"
	OUTPUT_OCS_02=$(ack "$1" $HOME/code/edk 2>&1)
	if $debug ; then echo "DEBUG[${OUTPUT_OCS_02}]" ; fi

echocolor "#OSC ED2K FXS"
	OUTPUT_OCS_03=$(ack "$1" $HOME/code/ed2k 2>&1)
	if $debug ; then echo "DEBUG[${OUTPUT_OCS_03}]" ; fi

echocolor "#Instance FXS ebury staging 1a"
	OUTPUT_INSTACE_EBURY_DEVEL_01=$(issh -C stagingfxs-1a "${COMMAND}" 2>&1)
	if $debug ; then echo "DEBUG[${OUTPUT_INSTACE_EBURY_DEVEL_01}]" ; fi
echocolor "#Instance FXS ebury staging 1b"
	OUTPUT_INSTACE_EBURY_DEVEL_02=$(issh -C stagingfxs-1b "${COMMAND}" 2>&1)
	if $debug ; then echo "DEBUG[${OUTPUT_INSTACE_EBURY_DEVEL_02}]" ; fi
echocolor "#Instance FXS ebury staging celery 1a"
	OUTPUT_INSTACE_EBURY_DEVEL_03=$(issh -C stagingfxscelery-1a "${COMMAND}" 2>&1)
	if $debug ; then echo "DEBUG[${OUTPUT_INSTACE_EBURY_DEVEL_03}]" ; fi

echocolor "#Instance FXS ebury prod 1a"
	OUTPUT_INSTACE_EBURY_PROD_01=$(issh -C prodfxs-1a "${COMMAND}" 2>&1)
	if $debug ; then echo "DEBUG[${OUTPUT_INSTACE_EBURY_PROD_01}]" ; fi
echocolor "#Instance FXS ebury prod 1b"
	OUTPUT_INSTACE_EBURY_PROD_02=$(issh -C prodfxs-1b "${COMMAND}" 2>&1)
	if $debug ; then echo "DEBUG[${OUTPUT_INSTACE_EBURY_PROD_02}]" ; fi
echocolor "#Instance FXS ebury prod celery 1a"
	OUTPUT_INSTACE_EBURY_PROD_03=$(issh -C prodfxscelery-1a "${COMMAND}" 2>&1)
	if $debug ; then echo "DEBUG[${OUTPUT_INSTACE_EBURY_PROD_03}]" ; fi

echocolor "#Instance FXS frontier 1a"
	OUTPUT_INSTACE_FRONTIER_PROD_01=$(issh -C prodthefxfirmfxs "${COMMAND}" 2>&1)
	if $debug ; then echo "DEBUG[${OUTPUT_INSTACE_EBURY_PROD_03}]" ; fi

echocolor "#Instance demo" 
	OUTPUT_INSTACE_EBURY_DEMO_01=$(issh -C bosdemo.eburypartners.com "${COMMAND}" 2>&1)
	if $debug ; then echo "DEBUG[${OUTPUT_INSTACE_EBURY_DEMO_01}]" ; fi


echocolor "#Instance api-sandbox"
	OUTPUT_INSTACE_API_DEMO_01=$(issh -C bos.ebury-api-edk-demo.ebury.rocks "${COMMAND}" 2>&1)
	if $debug ; then echo "DEBUG[${OUTPUT_INSTACE_API_DEMO_01}]" ; fi


echocolor "END"